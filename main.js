//Iteración #1: Mix for e includes

/*
const movies = [
    {title: 'Madaraspar', duration: 192, categories: ['comedia', 'aventura']},
    {title: 'Spiderpan', duration: 122, categories: ['aventura', 'acción']},
    {title: 'Solo en Whatsapp', duration: 223, categories: ['comedia', 'thriller']},
    {title: 'El gato con guantes', duration: 111, categories: ['comedia', 'aventura', 'animación']},
]
var categorias = [];

for (const i of movies) {
        i.categories.forEach(elemento => {
//             if(categorias.includes(elemento)){
//                console.log('Ya está dentro');
//          }else{
//              categorias.push(elemento)
//            }
            (categorias.includes(elemento)) ? console.log('Ya está dentro') : categorias.push(elemento);
        });
}
console.log(categorias);

*/

//**Iteración #2: Mix Fors**

/*
const users = [
    {name: 'Manolo el del bombo',
        favoritesSounds: {
            waves: {format: 'mp3', volume: 50},
            rain: {format: 'ogg', volume: 60},
            firecamp: {format: 'mp3', volume: 80},
        }
    },
    {name: 'Mortadelo',
        favoritesSounds: {
            waves: {format: 'mp3', volume: 30},
            shower: {format: 'ogg', volume: 55},
            train: {format: 'mp3', volume: 60},
        }
    },
    {name: 'Super Lopez',
        favoritesSounds: {
            shower: {format: 'mp3', volume: 50},
            train: {format: 'ogg', volume: 60},
            firecamp: {format: 'mp3', volume: 80},
        }
    },
    {name: 'El culebra',
        favoritesSounds: {
            waves: {format: 'mp3', volume: 67},
            wind: {format: 'ogg', volume: 35},
            firecamp: {format: 'mp3', volume: 60},
        }
    },
]

let totalVolume = 0;
let rounds = 0;
let average = 0;

users.forEach(element => {
    const sounds = element.favoritesSounds;
    // console.log(sounds);
    const list = Object.values(sounds);
    // console.log(list);
    list.forEach(elementTwo => {
    // console.log(elementTwo.volume);
        totalVolume += elementTwo.volume;
        rounds++;
    });
});

average = totalVolume/rounds;
console.log(`El promedio de los sonidos favoritos de los usuarios es de ${average}`);
*/


//Iteración #4: Métodos findArrayIndex

/*
const animals = ['Caracol', 'Mosquito', 'Salamandra', 'Ajolote'];
var animalText = 'Mosquito';

findArrayIndex = (array, text) => {
    animals.forEach(element => {
        if(element == animalText){
            return console.log(animals.indexOf(element));
        }
    });
}
findArrayIndex(animals,animalText)
*/

//Iteración #5: Función rollDice


/*
var randomNumber = 0;
rolldice = (paramOne, paramTwo) =>{
    randomNumber = Math.floor(Math.random()*(paramTwo-paramOne+1)+paramOne)
}
rolldice(1,6);
console.log(randomNumber);
*/

//Iteración #6: Función swap
